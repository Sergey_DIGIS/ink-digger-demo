export const MOCK_SESSIONS = [
  {
    date: new Date(2020, 11, 12, 9, 1),
    id: "SeJ6FbziYj8YXgG9mlPy",
    length: 6,
    name: "Sesja z Anią",
    place: "Wołodzyński",
    price: 2000,
    userId: "p65rWtt09dboUvFAqqDbUCNfZtt1"
  },
  {
    date: new Date(2020, 11, 4, 7, 12),
    id: "ZcsWgSAhcSo8bqpKCN3a",
    length: 3,
    name: "Z Beatą",
    place: "Qwerty",
    price: 250,
    userId: "p65rWtt09dboUvFAqqDbUCNfZtt1"
  },
  {
    date: new Date(2020, 11, 22, 21, 37),
    id: "l80S4qrmFzeIFkZWNOz7",
    length: 8,
    name: "Z Felixem",
    place: "Popes",
    price: 1300,
    userId: "p65rWtt09dboUvFAqqDbUCNfZtt1"
  },
  {
    date: new Date(2020, 11, 26, 16),
    id: "wloozWCUOX8FvXudRzYN",
    length: 1,
    name: "Z Anią",
    place: "Midnight",
    price: 5,
    userId: "p65rWtt09dboUvFAqqDbUCNfZtt1"
  },
  {
    date: new Date(2020, 11, 12, 1),
    id: "yx9HGjMTMGSCO5wFsLV7",
    length: 4,
    name: "Z Romkiem",
    place: "Na Qwadracie",
    price: 12,
    userId: "p65rWtt09dboUvFAqqDbUCNfZtt1"
  }
];

export const MOCK_DAYS_OFF = [
  {
    date: new Date(2020, 11, 5),
    dayOff: false,
    id: "0o5gS8dFGbDdCbVtLLdi",
    userId: "p65rWtt09dboUvFAqqDbUCNfZtt1"
  },
  {
    date: new Date(2020, 11, 7),
    dayOff: true,
    id: "1Ml4k5kAluLRGu2VS1LP",
    userId: "p65rWtt09dboUvFAqqDbUCNfZtt1"
  },
  {
    date: new Date(2020, 11, 21),
    dayOff: true,
    id: "7kjaTayalnTzvYAqQfwe",
    userId: "p65rWtt09dboUvFAqqDbUCNfZtt1"
  },
  {
    date: new Date(2020, 11, 23),
    dayOff: false,
    id: "AtGL7rUN6G4bJepRGNHA",
    userId: "p65rWtt09dboUvFAqqDbUCNfZtt1"
  },
  {
    date: new Date(2020, 11, 13),
    dayOff: false,
    id: "BuLljTG716KtwzbhL9UA",
    userId: "p65rWtt09dboUvFAqqDbUCNfZtt1"
  },
  {
    date: new Date(2020, 11, 15),
    dayOff: false,
    id: "GGjkruMNp0AekPXDu48T",
    userId: "p65rWtt09dboUvFAqqDbUCNfZtt1"
  }
];
