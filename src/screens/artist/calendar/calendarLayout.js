import React from "react";
import {View, StyleSheet, ScrollView} from "react-native";

import CalendarDayPicker from "components/calendars/calendarDayPicker";
import Reservations from "assets/svg/reservations.svg";
import CheckBox from "components/checkbox";
import SessionsList from "./sessionsList";
import Text from "components/text";
import NavBar from "components/navigation/navBar";

export const calendarLayout = ({
  selectedDate,
  isDayOffCheckBoxOn,
  onDaySelect,
  daysOff,
  sessions,
  onDayOffCheckBoxChange,
  displayedSessions,
  areUndisplayedReservations
}) => (
  <View style={styles.container}>
    <ScrollView>
      <View style={{flex: 1}}>
        <NavBar
          rightIcon={Reservations}
          isRightIconMarked={areUndisplayedReservations}
          title="Kalendarz"
        />
        <CalendarDayPicker
          daysOff={daysOff}
          sessions={sessions}
          selectedDay={selectedDate}
          onSelectedDayChange={onDaySelect}
          areDisabledDaysPressable
          darkMode
        />

        <View style={styles.dayOffSwitchContainer}>
          <CheckBox
            value={isDayOffCheckBoxOn}
            onValueChange={onDayOffCheckBoxChange}
          />
          <Text style={{marginLeft: 15}}>
            {isDayOffCheckBoxOn ? "Dziś odpoczywam" : "Ustaw dzień wolny"}
          </Text>
        </View>

        {displayedSessions.length ? (
          <SessionsList sessions={displayedSessions} />
        ) : (
          <Text style={styles.noSessionsMessage}>Brak umówionych sesji</Text>
        )}
      </View>
    </ScrollView>
  </View>
);

const styles = StyleSheet.create({
  container: {
    paddingLeft: 16,
    paddingRight: 16,
    backgroundColor: "#080825",
    height: "100%",
    justifyContent: "space-between"
  },
  dayOffSwitchContainer: {
    flexDirection: "row",
    alignItems: "center",
    marginBottom: 24,
    opacity: 0.9
  },
  noSessionsMessage: {
    textAlign: "center",
    margin: 24,
    opacity: 0.9
  }
});
