import {getDayPartFromDate} from "../../../utils/date";
import {setDayOff} from "../../../utils/dayOff";
import {calendarLayout} from "./calendarLayout";
import {connect} from "react-redux";
import {Component} from "react";

class CalendarScreen extends Component {
  state = {
    selectedDate: new Date(),
    isSelectedDateDisabled: false,
    isDayOffCheckBoxOn: null,
    displayedSessions: [],
    disabledWeekDays: {}
  };

  onDaySelect = (selectedDate, marking) => {
    const displayedSessions = this.props.sessions.filter(session => {
      const trueSessionDate = session.proposedChanges
        ? session.proposedChanges.date
        : session.date;

      const sessionDayTime = getDayPartFromDate(trueSessionDate).getTime();
      const selectedDayTime = getDayPartFromDate(selectedDate).getTime();

      return sessionDayTime === selectedDayTime;
    });

    this.setState({
      selectedDate,
      isDayOffCheckBoxOn: marking.disabled,
      displayedSessions
    });
  };

  onDayOffCheckBoxChange = newDayOff => {
    const {selectedDate} = this.state;
    this.setState({isDayOffCheckBoxOn: newDayOff});

    return setDayOff(selectedDate, newDayOff).catch(error => {
      console.warn(error);

      this.setState(prevState => {
        if (selectedDate !== prevState.selectedDate) return;
        return {isDayOffCheckBoxOn: !newDayOff};
      });
    });
  };

  render() {
    const {displayedSessions, selectedDate, isDayOffCheckBoxOn} = this.state;
    const {sessions, daysOff} = this.props;

    return calendarLayout({
      selectedDate,
      onDaySelect: this.onDaySelect,
      isDayOffCheckBoxOn,
      daysOff,
      sessions,
      onDayOffCheckBoxChange: this.onDayOffCheckBoxChange,
      displayedSessions,
      areUndisplayedReservations: true
    });
  }
}

const mapStateToProps = state => {
  const {sessions, daysOff} = state.calendar;
  return {sessions, daysOff};
};

export default connect(mapStateToProps)(CalendarScreen);
