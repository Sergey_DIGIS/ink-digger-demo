import {View, StyleSheet, ScrollView} from "react-native";

import {formatDateToString} from "utils/date";
import Button from "components/buttons/button";
import EventDetails from "./components";
import React, {useState} from "react";
import Text from "components/text";

const SessionDetails = ({navigation, route}) => {
  const {session} = route.params;

  const [areDetailsHidden, setAreDetailsHidden] = useState(true);

  const footerActions = session.proposedChanges ? (
    <Button
      type="primary"
      title="Prośba wysłana"
      disabled
      containerStyle={{flex: 1}}
    />
  ) : (
    <Button
      dark
      title="Akceptuj"
      containerStyle={{flex: 1}}
      onPress={navigation.goBack}
    />
  );

  return (
    <ScrollView contentContainerStyle={{flexGrow: 1}}>
      <EventDetails
        title="Szczegóły sesji"
        onGoBackRequest={navigation.goBack}
        footer={footerActions}
      >
        {session.proposedChanges && (
          <View style={styles.proposedContainer}>
            <EventDetails.Prop
              title="Nowy termin"
              value={formatDateToString(session.proposedChanges.date)}
              fullWidth
              dark
            />
            <View style={styles.waitingStatusContainer}>
              <Text dark>oczekuje</Text>
              <View style={styles.statusDot} />
            </View>
          </View>
        )}

        {session.proposedChanges && areDetailsHidden && (
          <Button
            dark
            title="Pokaż szczegóły sesji"
            onPress={() => setAreDetailsHidden(false)}
          />
        )}

        {(!session.proposedChanges || !areDetailsHidden) && (
          <View style={styles.contentContainer}>
            <EventDetails.PropsRow>
              <EventDetails.Prop title="Klient" value={session.name} dark />
              <EventDetails.Prop
                title="Cena"
                value={session.price + " pln"}
                dark
              />
            </EventDetails.PropsRow>
            <EventDetails.PropsRow>
              <EventDetails.Prop
                title="Studio/spot"
                value={session.place}
                dark
              />
              <EventDetails.Prop
                title="Termin"
                value={session.date}
                dark
              />
            </EventDetails.PropsRow>
            <EventDetails.PropsRow>
              <EventDetails.Prop
                title="Czas trwania"
                value={session.length + "h"}
                dark
              />
            </EventDetails.PropsRow>
          </View>
        )}
      </EventDetails>
    </ScrollView>
  );
};



const styles = StyleSheet.create({
  proposedContainer: {
    flexDirection: "row",
    alignItems: "flex-end"
  },
  waitingStatusContainer: {
    flexDirection: "row",
    alignItems: "center",
    marginBottom: 18
  },
  statusDot: {
    width: 8,
    height: 8,
    backgroundColor: "#FFB008",
    borderRadius: 4,
    marginLeft: 8
  },
  contentContainer: {
    marginTop: 30
  }
});

export default SessionDetails;
