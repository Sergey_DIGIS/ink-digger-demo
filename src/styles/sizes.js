import {Dimensions} from "react-native";

import {getSize} from "utils/screenSize";

// Retrieve initial screen's width
let screenWidth = Dimensions.get("window").width;

let screenHeight = Dimensions.get("window").height;

export const isSmall = screenHeight / screenWidth <= 1.8;

const SMALL_ICON_SA = 24;
const MEDIUM_ICON_SA = 28;
const LARGE_ICON_SA = 32;

export const iconSafeAreaSize = getSize(
  SMALL_ICON_SA,
  MEDIUM_ICON_SA,
  LARGE_ICON_SA
);
