import {Dimensions, Platform} from "react-native";
import {iconSafeAreaSize} from "styles/sizes";

export function isIphoneX() {
  const iphoneXLength = 812;
  const iphoneXSMaxLength = 896;
  const windowDimensions = Dimensions.get("window");
  return (
    Platform.OS === "ios" &&
    !Platform.isPad &&
    !Platform.isTVOS &&
    (windowDimensions.width === iphoneXLength ||
      windowDimensions.height === iphoneXLength ||
      windowDimensions.width === iphoneXSMaxLength ||
      windowDimensions.height === iphoneXSMaxLength)
  );
}

export const containerPaddingX = 16;
export const viewMarginBottom = Platform.OS === "ios" && isIphoneX() ? 0 : 24;
export const viewMarginBottomWithoutSafeArea = isIphoneX() ? 35 : 24;
export const viewMarginTop = 0;
// 0 because of Safe Area View support
export const containerPaddingnTop = 8;

export const container = {
  paddingLeft: containerPaddingX,
  paddingRight: containerPaddingX,
  width: "100%"
};

export const clickableAreaIcon = {
  top: iconSafeAreaSize,
  bottom: iconSafeAreaSize,
  left: iconSafeAreaSize,
  right: iconSafeAreaSize
};
