import {View, StyleSheet, TouchableWithoutFeedback} from "react-native";
import CheckWhite from "assets/svg/check.svg";
import * as Colors from "styles/colors";
import PropTypes from "prop-types";
import React from "react";

const CheckBox = ({value, onValueChange, style}) => {
  const handlePress = () => onValueChange && onValueChange(!value);

  return (
    <TouchableWithoutFeedback onPress={handlePress}>
      <View>
        <View style={styles.clickableArea} />
        <View style={[styles.checkBox, value && styles.checkBoxActive, style]}>
          {value && <CheckWhite height={16} style={styles.icon} />}
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
};

CheckBox.propTypes = {
  value: PropTypes.bool,
  onValueChange: PropTypes.func,
  styles: PropTypes.object
};

const styles = StyleSheet.create({
  clickableArea: {
    width: 48,
    height: 48,
    top: -12,
    left: -12,
    position: "absolute"
  },
  checkBox: {
    justifyContent: "center",
    alignItems: "center",

    width: 23,
    height: 23,
    borderRadius: 3,
    borderWidth: 2,
    borderColor: Colors.gold
  },
  checkBoxActive: {
    backgroundColor: Colors.gold
  },
  icon: {
    color: Colors.white
  }
});

export default CheckBox;
