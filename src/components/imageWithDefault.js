import React, {useState, useEffect} from "react";
import PropTypes from "prop-types";
import {Image} from "react-native";

const ImageWithDefault = ({source, defaultImage, onError, ...restProps}) => {
  const [isError, setIsError] = useState(false);

  useEffect(() => {
    if (source.uri) {
      setIsError(false);
    } else {
      handleOnError();
    }
  }, [source.uri]);

  const handleOnError = () => {
    setIsError(true);
    onError && onError();
  };

  return (
    <Image
      source={isError ? defaultImage : source}
      onError={handleOnError}
      {...restProps}
    />
  );
};

ImageWithDefault.propTypes = {
  source: PropTypes.object,
  defaultImage: PropTypes.node
};

export default ImageWithDefault;
