import {formatDateToCalendarFormat} from "../../utils/calendar";
import SwipeCalendar from "./swipeCalendar";
import {pipe} from "../../utils/helpers";
import React, {useMemo} from "react";
import PropTypes from "prop-types";

const Calendar = ({
  markedDates,
  disabledWeekDays,
  sessions,
  daysOff,
  availability,
  ...restProps
}) => {
  const artistMarkedDates = useMemo(() => {
    if (!sessions && !daysOff) return;

    return pipe(
      applySessionsToMarkedDates(sessions),
      applyDaysOffToMarkedDates(daysOff)
    )({});
  }, [sessions, daysOff]);

  const mergedMarkedDates = useMemo(() => {
    if (!artistMarkedDates) return markedDates;
    if (!markedDates) return artistMarkedDates;

    const newMarkedDates = {...artistMarkedDates};

    Object.entries(markedDates).forEach(([dateString, marking]) => {
      newMarkedDates[dateString] = {
        ...newMarkedDates[dateString],
        ...marking
      };
    });

    return newMarkedDates;
  }, [artistMarkedDates, markedDates]);

  const mergedDisabledWeekDays = useMemo(() => {
    if (!availability) return disabledWeekDays;

    return {
      ...mapArtistAvailabilityToDisabledWeekDays(availability),
      ...disabledWeekDays
    };
  }, [disabledWeekDays, availability]);

  return (
    <SwipeCalendar
      {...restProps}
      markedDates={mergedMarkedDates}
      disabledWeekDays={mergedDisabledWeekDays}
    />
  );
};

const applySessionsToMarkedDates = sessions => markedDates => {
  if (!sessions) return markedDates;

  const newMarkedDates = {...markedDates};
  sessions.forEach(session => {
    const trueDate = session.proposedChanges
      ? session.proposedChanges.date
      : session.date;
    const selectedDayText = formatDateToCalendarFormat(trueDate);

    newMarkedDates[selectedDayText] = {
      ...newMarkedDates[selectedDayText],
      dot: true,
      activeIcon: !!session.proposedChanges
    };
  });

  return newMarkedDates;
};

const applyDaysOffToMarkedDates = daysOff => markedDates => {
  if (!daysOff) return markedDates;

  const newMarkedDates = {...markedDates};
  daysOff.forEach(item => {
    const dayOffText = formatDateToCalendarFormat(item.date);

    newMarkedDates[dayOffText] = {
      ...newMarkedDates[dayOffText],
      disabled: !!item.dayOff
    };
  });

  return newMarkedDates;
};

const mapArtistAvailabilityToDisabledWeekDays = activity => {
  const mappedEntries = Object.entries(activity).map(([dayName, value]) => [
    dayName,
    value.dayOff
  ]);
  return Object.fromEntries(mappedEntries);
};

Calendar.propTypes = {
  ...SwipeCalendar.propTypes,
  markedDates: PropTypes.object,
  disabledWeekDays: PropTypes.object,
  sessions: PropTypes.array,
  daysOff: PropTypes.array,
  availability: PropTypes.object
};

export default Calendar;
