import {Calendar as RNCalendar} from "react-native-calendars";
import DayItem from "./components/dayItem";
import {DAY_NAMES} from "constants/date";
import React, {useCallback} from "react";
import {Colors, Typography} from "styles/index";
import PropTypes from "prop-types";

const HeadlessCalendar = ({
  darkMode,
  disabledWeekDays,
  onPressDay,
  areDisabledDaysPressable,
  ...restProps
}) => {
  const renderDayComponent = useCallback(
    ({date, state, marking}) => {
      const weekDayName = DAY_NAMES[new Date(date.timestamp).getDay()];
      const isWeekDayDisabled = !!(
        disabledWeekDays && disabledWeekDays[weekDayName]
      );
      const disabled =
        state === "disabled" || marking.disabled === undefined
          ? isWeekDayDisabled
          : marking.disabled;

      const fixedMarking = {...marking, disabled};

      return (
        <DayItem
          style={{backgroundColor: Colors.goldBorder}}
          date={date}
          darkMode={darkMode}
          isDisabled={disabled}
          isPressDisabled={!areDisabledDaysPressable && disabled}
          isToday={state === "today"}
          isSelected={marking.selected}
          hasDot={marking.dot}
          hasStar={marking.star}
          hasActiveIcon={marking.activeIcon}
          isPeriodStart={marking.periodStart}
          isPeriodEnd={marking.periodEnd}
          isInPeriod={marking.periodIn}
          onPress={dateObj => onPressDay(dateObj, fixedMarking)}
        />
      );
    },
    [darkMode, disabledWeekDays, onPressDay, areDisabledDaysPressable]
  );

  return (
    <RNCalendar
      markingType="custom"
      theme={{
        backgroundColor: "transparent",
        calendarBackground: "transparent",
        textMonthFontFamily: Typography.fontFamilyBase,
        textDayHeaderFontFamily: Typography.fontFamilyBase,
        "stylesheet.calendar.header": {
          header: {
            display: "none"
          }
        }
      }}
      dayComponent={renderDayComponent}
      disableMonthChange
      hideExtraDays
      disabledDaysIndexes={[1, 3]}
      {...restProps}
    />
  );
};

HeadlessCalendar.propTypes = {
  darkMode: PropTypes.bool,
  disabledWeekDays: PropTypes.object,
  onPressDay: PropTypes.func,
  areDisabledDaysPressable: PropTypes.bool
};

export default HeadlessCalendar;
