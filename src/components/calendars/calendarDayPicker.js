import {formatDateToCalendarFormat} from "../../utils/calendar";
import React, {useCallback, useMemo} from "react";
import PropTypes from "prop-types";
import Calendar from "./calendar";

const CalendarDayPicker = ({
  markedDates,
  selectedDay,
  onSelectedDayChange,
  ...restProps
}) => {
  const selectedDayText =
    selectedDay && formatDateToCalendarFormat(selectedDay);

  const handleDayPress = useCallback(
    (date, marking) => {
      onSelectedDayChange &&
        onSelectedDayChange(new Date(date.timestamp), marking);
    },
    [onSelectedDayChange]
  );

  const datesWithSelectedDate = useMemo(() => {
    if (!selectedDayText) return markedDates;
    const fixedMarkedDates = markedDates || {};

    return {
      ...fixedMarkedDates,
      [selectedDayText]: {
        ...fixedMarkedDates[selectedDayText],
        selected: true
      }
    };
  }, [markedDates, selectedDayText]);

  return (
    <Calendar
      {...restProps}
      onPressDay={handleDayPress}
      markedDates={datesWithSelectedDate}
    />
  );
};

CalendarDayPicker.propTypes = {
  ...Calendar.propTypes,
  markedDates: PropTypes.object,
  selectedDay: PropTypes.object,
  onSelectedDayChange: PropTypes.func
};

export default CalendarDayPicker;
