import {View} from "react-native";
import React from "react";

export const ClickableArea = ({...rest}) => (
  <View
    {...rest}
    style={{
      width: 48,
      height: 48,
      top: -12,
      left: -12,
      position: "absolute"
    }}
  />
);
