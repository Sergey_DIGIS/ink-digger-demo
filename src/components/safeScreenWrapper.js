import {SafeAreaView, StatusBar, StyleSheet} from "react-native";
import * as Colors from "styles/colors";
import React from "react";

const SafeScreenWrapper = ({children}) => (
  <SafeAreaView style={styles.droidSafeArea}>{children}</SafeAreaView>
);

const styles = StyleSheet.create({
  droidSafeArea: {
    flex: 1,
    backgroundColor: Colors.bg,
    paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0
  }
});

export default SafeScreenWrapper;
