import {Colors, Typography} from "styles/index";
import {Text as RNText} from "react-native";
import PropTypes from "prop-types";
import React from "react";

const FONT_FAMILIES = {
  brand: Typography.fontFamilyBrand,
  base: Typography.fontFamilyBase,
  baseBold: Typography.fontFamilyBaseBold
};
const FONT_SIZES = {
  large: Typography.largeFontSize,
  base: Typography.baseFontSize,
  small: Typography.smallFontSize
};
const COLORS = {
  white: Colors.white,
  dark: Colors.navyBlue
};
const OPACITY = 0.4;

const Text = ({
  family = "base",
  size = "base",
  dark,
  withOpacity,
  style,
  ...restProps
}) => {
  const computedStyle = {
    fontFamily: FONT_FAMILIES[family] || family,
    fontSize: FONT_SIZES[size] || size,
    color: dark ? COLORS.dark : Colors.white,
    opacity: withOpacity ? OPACITY : 1
  };

  return <RNText style={[computedStyle, style]} {...restProps} />;
};

Text.propTypes = {
  family: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.oneOf(["brand", "base", "baseBold"])
  ]),
  size: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.oneOf(["large", "base", "small"])
  ]),
  dark: PropTypes.bool,
  withOpacity: PropTypes.bool
};

export default React.memo(Text);
