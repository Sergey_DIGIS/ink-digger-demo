import {TouchableWithoutFeedback, StyleSheet, View} from "react-native";
import PropTypes from "prop-types";
import React from "react";

const ActionIconButton = ({icon: Icon, onPress}) => {
  return (
    <TouchableWithoutFeedback onPress={onPress}>
      <View style={styles.topBarIcon}>
        <Icon height="24" width="24" />
      </View>
    </TouchableWithoutFeedback>
  );
};

ActionIconButton.propTypes = {
  icon: PropTypes.func.isRequired,
  onPress: PropTypes.func
};

const styles = StyleSheet.create({
  topBarIcon: {
    padding: 10,
    marginBottom: -10
  }
});

export default ActionIconButton;
