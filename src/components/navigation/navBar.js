import React from "react";
import PropTypes from 'prop-types';
import {StyleSheet, View} from "react-native";

import {iconSafeAreaSize} from "styles/sizes";
import IconButton from "components/buttons/iconButton";
import Text from "components/text";
import * as Typography from "styles/typography";

const NavBar = ({
  leftIcon,
  rightIcon,
  isLeftIconMarked,
  isRightIconMarked,
  title,
  ...props
}) => {
  return (
    <View
      style={[
        styles.container,
        {paddingLeft: !leftIcon ? iconSafeAreaSize : 0}
      ]}
      {...props}
    >
      {!!leftIcon && (
        <IconButton icon={leftIcon} marked={isLeftIconMarked} />
      )}
      <Text family="brand" style={styles.title}>
        {title}
      </Text>
      <IconButton icon={rightIcon} marked={isRightIconMarked} />
    </View>
  );
};

NavBar.propTypes = {
  rightIcon: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
  leftIcon: PropTypes.func,
  isLeftIconMarked: PropTypes.bool,
  isRightIconMarked: PropTypes.bool,
};

export default NavBar;

const styles = StyleSheet.create({
  container: {
    height: 44,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    marginBottom: 24
  },
  title: {
    flex: 1,
    textAlign: "center",
    fontSize: 18,
    height: 23,
    ...Typography.naviText
  }
});
