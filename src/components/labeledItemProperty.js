import {View, StyleSheet} from "react-native";
import PropTypes from "prop-types";
import Text from "components/text";
import {Colors} from "styles";
import React from "react";

const LabeledItemProperty = ({title, value, dark, style, ...restProps}) => {
  return (
    <View style={[styles.container, style]} {...restProps}>
      <Text dark={dark} size="small" style={{opacity: 0.8}}>
        {title}
      </Text>
      <View style={styles.divider} />
      <Text dark={dark} style={styles.value}>
        {value}
      </Text>
    </View>
  );
};

LabeledItemProperty.propTypes = {
  title: PropTypes.string,
  value: PropTypes.any,
  dark: PropTypes.bool,
  style: PropTypes.object
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginBottom: 18
  },

  divider: {
    width: 16,
    height: 2,
    backgroundColor: Colors.gold,
    marginVertical: 4,
    borderRadius: 1,
    marginBottom: 8
  }
});

export default React.memo(LabeledItemProperty);
