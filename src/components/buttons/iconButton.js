import {TouchableOpacity, View, StyleSheet} from "react-native";
import {Colors} from "styles/index";
import PropTypes from "prop-types";
import React from "react";
import {clickableAreaIcon} from "styles/spacing";
import {iconSafeAreaSize} from "styles/sizes";

const IconButton = ({
  icon: Icon,
  marked,
  width,
  height,
  color = "white",
  onPress
}) => {
  return (
    <TouchableOpacity onPress={onPress} style={{width, height}} hitSlop={clickableAreaIcon}>
      <Icon width={width} height={height} color={color} />
      {marked && <View style={styles.dot} />}
    </TouchableOpacity>
  );
};

IconButton.propTypes = {
  icon: PropTypes.func,
  marked: PropTypes.bool,
  width: PropTypes.number,
  height: PropTypes.number,
  color: PropTypes.string,
  onPress: PropTypes.func
};

IconButton.defaultProps = {
  width: iconSafeAreaSize,
  height: iconSafeAreaSize
};

const styles = StyleSheet.create({
  dot: {
    position: "absolute",
    top: 0,
    right: 0,
    width: 6,
    height: 6,
    borderRadius: 3,
    backgroundColor: Colors.gold
  }
});

export default IconButton;
