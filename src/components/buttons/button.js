import {StyleSheet, TouchableOpacity, ActivityIndicator} from "react-native";
import * as Colors from "styles/colors";
import PropTypes from "prop-types";
import Text from "components/text";
import React from "react";

const Button = ({
  type,
  title,
  dark,
  icon: Icon,
  onPress,
  disabled,
  loading,
  containerStyle,
  titleStyle,
  iconStyle,
  topLeftRadius,
  topRightRadius,
  bottomLeftRadius,
  bottomRightRadius
}) => {
  const isPrimary = type === "primary";
  const isOutlined = type === "outlined";

  const mergedContainerStyle = [
    styles.container,
    isPrimary && styles.containerPrimary,
    isPrimary && (disabled || loading) && styles.containerPrimaryDisabled,
    isOutlined && styles.containerOutlined,
    topLeftRadius && styles.topLeftRadius,
    topRightRadius && styles.topRightRadius,
    bottomLeftRadius && styles.bottomLeftRadius,
    bottomRightRadius && styles.bottomRightRadius,
    containerStyle
  ];
  const mergedIconStyle = [title && styles.iconWithTitle, iconStyle];
  const mergedTitleStyle = [styles.title, titleStyle];

  const indicatorColor = isPrimary ? Colors.white : Colors.navyBlue;

  return (
    <TouchableOpacity
      style={mergedContainerStyle}
      onPress={onPress}
      activeOpacity={0.5}
      disabled={disabled || loading}
    >
      {loading && <ActivityIndicator size="small" color={indicatorColor} />}
      {!loading && Icon && <Icon height={24} style={mergedIconStyle} />}
      {!loading && title && (
        <Text family="brand" dark={!isPrimary && dark} style={mergedTitleStyle}>
          {title}
        </Text>
      )}
    </TouchableOpacity>
  );
};

Button.propTypes = {
  type: PropTypes.oneOf(["primary", "outlined"]),
  title: PropTypes.string,
  dark: PropTypes.bool,
  icon: PropTypes.func,
  onPress: PropTypes.func,
  disabled: PropTypes.bool,
  loading: PropTypes.bool,
  containerStyle: PropTypes.object,
  titleStyle: PropTypes.object,
  iconStyle: PropTypes.object,
  topLeftRadius: PropTypes.bool,
  topRightRadius: PropTypes.bool,
  bottomLeftRadius: PropTypes.bool,
  bottomRightRadius: PropTypes.bool
};

const styles = StyleSheet.create({
  container: {
    padding: 7,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    borderWidth: 1,
    borderColor: "transparent",
    height: 40
  },
  containerPrimary: {
    backgroundColor: Colors.gold,
    borderColor: Colors.goldBorder
  },
  containerOutlined: {
    borderColor: Colors.goldBorder
  },
  containerPrimaryDisabled: {
    backgroundColor: Colors.goldGrayed,
    borderColor: Colors.goldGrayed
  },

  title: {
    textAlign: "center"
  },

  iconWithTitle: {
    marginRight: 5
  },

  topLeftRadius: {
    borderTopLeftRadius: 8
  },
  topRightRadius: {
    borderTopRightRadius: 8
  },
  bottomLeftRadius: {
    borderBottomLeftRadius: 8
  },
  bottomRightRadius: {
    borderBottomRightRadius: 8
  }
});

export default Button;
