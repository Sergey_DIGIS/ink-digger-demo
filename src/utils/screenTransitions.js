import {TransitionSpecs} from "@react-navigation/stack";

export const FORM_TRANSITION_CONFIG = {
  transitionSpec: {
    open: TransitionSpecs.TransitionIOSSpec,
    close: TransitionSpecs.TransitionIOSSpec
  },
  mode: "modal",
  gestureEnabled: true,
  gestureDirection: "vertical",
  gestureResponseDistance: {
    vertical: 150
  },
  cardOverlayEnabled: true,
  cardStyle: {backgroundColor: "transparent"},
  cardStyleInterpolator: ({current, next, layouts}) => ({
    cardStyle: {
      transform: [
        {
          translateY: current.progress.interpolate({
            inputRange: [0, 1],
            outputRange: [layouts.screen.height, 0]
          })
        },
        {
          scale: next
            ? next.progress.interpolate({
                inputRange: [0, 1],
                outputRange: [1, 0.9]
              })
            : 1
        }
      ]
    },
    overlayStyle: {
      opacity: current.progress.interpolate({
        inputRange: [0, 1],
        outputRange: [0, 0.5]
      })
    }
  })
};
