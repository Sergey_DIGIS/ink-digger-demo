import momentPl from "moment/src/locale/pl";
import moment from "moment";
import React from "react";

moment.updateLocale("pl", momentPl);

export const getDayPartFromDate = date =>
  new Date(date.getFullYear(), date.getMonth(), date.getDate());

export const addMonthsToDate = (date, monthsToAdd) => {
  let year = date.getFullYear();
  let month = date.getMonth();

  month += monthsToAdd;

  while (month >= 12) {
    month -= 12;
    year += 1;
  }
  while (month < 0) {
    month += 12;
    year -= 1;
  }

  const newDate = new Date();
  newDate.setDate(1);
  newDate.setMonth(month);
  newDate.setFullYear(year);

  return newDate;
};

export const formatDateToString = date =>
  moment(date).format("ddd. D MMMM, YYYY, HH:mm");
